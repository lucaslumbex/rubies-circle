import Vue from 'vue'
import moment from 'moment'


// create a new axios instance

Vue.filter('moneyFormat', value => {
    if (!value) return '';
    return value.toLocaleString()
});

Vue.filter('formatDateRelatively', value => {
    if (value) {
        let diff = moment(value).diff(moment(), 'milliseconds');
        let duration = moment.duration(diff);

        if (duration.days() === 0) {
            return 'Today'
        } else if (duration.days() === -1) {
            return 'Yesterday'
        } else {
            // return moment(String(value)).format('Do LL')
            return moment(String(value)).format('DD-MMM-YYYY')
        }
    }
});

Vue.filter('firstNameFromFullName', value => {
    if (!value) return '';
    value = value.toString();
    return value.split(' ').slice(0, -1).join(' ').toLowerCase()
});

Vue.filter('toUppercase', value => {
    if (!value) return '';
    value = value.toUpperCase();
    return value
});

Vue.filter('toLowercase', value => {
    if (!value) return '';
    value = value.toLowerCase();
    return value
});

Vue.filter('firstCaseCapital', value => {
    if (!value) return '';
    value = value.charAt(0).toUpperCase() + value.slice(1);
    return value
});

Vue.filter('moneyToDecimal', value => {
    if (!value) return '';
    // value = value.charAt(0).toUpperCase() + value.slice(1);
    value = parseFloat(value).toFixed(2);
    return value
});


// export default instance