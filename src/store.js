import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    networkError: false,
    appLoading: false,
    tasks: false,

  },
  getters:{
    getNetworkError: state => {
      return state.networkError
    },
    getAppLoading: state => {
      return state.appLoading
    },
    getTasks: state => {
      return state.tasks
    },
  },
  mutations: {
    updateNetworkError: (state, payload)=>{
      state.networkError = payload
    },
    updateAppLoading: (state, payload)=>{
      state.appLoading = payload
    },
    updateTasks: (state, payload)=>{
      state.tasks = payload
    },
  },
  actions: {
    updateNetworkError: ({ commit }, payload)=>{
      commit('updateNetworkError', payload)
    },
    updateAppLoading: ({ commit }, payload)=>{
      commit('updateAppLoading', payload)
    },
    updateTasks: ({ commit }, payload)=>{
      commit('updateTasks', payload)
    },
  }
})
