import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import KeenUI from 'keen-ui';
import 'keen-ui/dist/keen-ui.css';

import axios from 'axios'

Vue.config.productionTip = false;

import './filter';

Vue.use(KeenUI);
window.axios = axios;
axios.defaults.baseURL = 'https://lke14551sj.execute-api.us-east-2.amazonaws.com/dev';
// axios.defaults.baseURL = 'https://lke14551sj.execute-api.us-east-2.amazonaws.com/prod';

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

